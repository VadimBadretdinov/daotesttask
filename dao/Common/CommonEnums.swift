//
//  CommonEnums.swift
//  dao
//
//  Created by vadim on 23/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

enum TodayWeather: String {
    case Rain, Clouds, Clear, Mist, Thunderstorm, Snow
}

enum WeatherTime {
    case Night, Day
}
