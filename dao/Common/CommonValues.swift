//
//  API.swift
//  dao
//
//  Created by vadim on 23/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

enum CommonValues {
    
    static let ApiKey = "6d0a3e9e3cb69799269069ad9131332f"
    
    static let KelvinToCelsius = 275.15
}
