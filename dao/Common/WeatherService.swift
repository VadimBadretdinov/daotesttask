//
//  WeatherService.swift
//  dao
//
//  Created by vadim on 24/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit

class WeatherService {
    
    func getWeatherIcon(weather: TodayWeather, time: WeatherTime) -> UIImage? {
        if let image = UIImage(named: "\(weather)\(time)") {
            return image
        }
        return nil
    }
}
