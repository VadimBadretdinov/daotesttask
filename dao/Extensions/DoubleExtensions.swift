//
//  DoubleExtensions.swift
//  dao
//
//  Created by vadim on 03/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation
extension Double {
    func toCelsiusIntValue() -> Int {
        return Int(self - CommonValues.KelvinToCelsius)
    }
}

