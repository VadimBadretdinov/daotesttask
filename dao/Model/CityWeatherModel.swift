//
//  TownWeatherModel.swift
//  dao
//
//  Created by vadim on 23/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit
import Alamofire

class CityWeatherModel {
    fileprivate var cities: [CityObject] = [
        CityObject(city: "Kazan", lon: 49.12, lat: 55.78, weather: nil, weekWeather: nil, currentLocation: false),
        CityObject(city: "London", lon: -0.13, lat: 51.51, weather: nil, weekWeather: nil, currentLocation: false),
        CityObject(city: "Manchester", lon: -71.45, lat: 43, weather: nil, weekWeather: nil, currentLocation: false),
        CityObject(city: "Moscow", lon: 37.62, lat: 55.75, weather: nil, weekWeather: nil, currentLocation: false),
        CityObject(city: "Ulyanovsk", lon: 48.36, lat: 54.31, weather: nil, weekWeather: nil, currentLocation: false)
    ]
    
    func getCities() -> [CityObject] {
        return self.cities
    }
    
    func appendCurrentCity(city: CityObject) {
        if let cityString = city.city, !self.cities.contains(where: { (dbobj) -> Bool in
            dbobj.city == cityString
        }) {
            if let firstCity = self.cities.first, !firstCity.currentLocation {
                self.cities.insert(city, at: 0)
            } else {
                self.cities[0] = city
            }
        }
    }
    
    func updateCity(update: CityObject) -> [CityObject]?{
        if let updateCity = update.city {
            for i in 0..<self.cities.count {
                if self.cities[i].city == updateCity {
                    self.cities[i] = update
                    return self.cities
                }
            }
        }
        return nil
    }
    
    func fetchCurrentWeather(lon: Double, lat: Double, completion: @escaping (_ weather: CityWeather?) -> ()) {
        if let url = URL(string: StringUrl.day(lat: lat, lon: lon).rawValue) {
            AF.request(url).responseJSON { response in
                do {
                    if let data = response.data {
                        let decoder = JSONDecoder()
                        let weather = try decoder.decode(CityWeather.self, from: data)
                        completion(weather)
                    } else {
                        completion(nil)
                    }
                } catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    
    func fetchWeekWeather(lon: Double, lat: Double, completion: @escaping (_ weather: CityWeatherForecast?) -> ()) {
        if let url = URL(string: StringUrl.week(lat: lat, lon: lon).rawValue) {
            AF.request(url).responseJSON { response in
                do {
                    if let data = response.data {
                        let decoder = JSONDecoder()
                        let weather = try decoder.decode(CityWeatherForecast.self, from: data)
                        completion(weather)
                    } else {
                        completion(nil)
                    }
                } catch {
                    print("Error: \(error)")
                }
            }
        }
    }
}

fileprivate enum StringUrl {
    init?(rawValue: String) {return nil}
    
    var rawValue: String {
        switch self {
        case .day(let lat, let lon):
            return "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(CommonValues.ApiKey)"
        case .week(let lat, let lon):
            return "https://api.openweathermap.org/data/2.5/forecast?lat=\(lat)&lon=\(lon)&appid=\(CommonValues.ApiKey)"
        }
    }
    
    typealias RawValue = String
    
    case day(lat: Double, lon: Double)
    case week(lat: Double, lon: Double)
}
