//
//  ParseObject.swift
//  dao
//
//  Created by vadim on 05/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation
import SwiftDate

class CityObject: Codable {
    var city: String?
    var lon: Double?
    var lat: Double?
    var weather: CityWeather?
    var weekWeather: CityWeatherForecast?
    let currentLocation: Bool
    var day: Int?
    
    init(city: String?, lon: Double?, lat: Double?, weather: CityWeather?, weekWeather: CityWeatherForecast?, currentLocation: Bool) {
        self.city = city
        self.lon = lon
        self.lat = lat
        self.currentLocation = currentLocation
        self.weather = weather
        self.weekWeather = weekWeather
    }
}
