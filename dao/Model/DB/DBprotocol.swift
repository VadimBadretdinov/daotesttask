//
//  File.swift
//  dao
//
//  Created by vadim on 04/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

protocol DBprotocol {
    func save()
    func load()
    func delete()
}
