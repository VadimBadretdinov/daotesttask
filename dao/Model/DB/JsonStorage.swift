//
//  JSON.swift
//  dao
//
//  Created by vadim on 04/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

class JsonStorage:DBprotocol {
    
    static let shared = JsonStorage()
    
    func delete() {
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileManager = FileManager.default
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("json.txt")
        do {
            try fileManager.removeItem(atPath: fileUrl.path)
        } catch {
            print("JSON removing data error: \(error)")
        }
    }
    
    func load() {
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("json.txt")
        do {
            let jsonString = try String(contentsOf: fileUrl, encoding: .utf8)
            let decoder = JSONDecoder()
            let data = jsonString.data(using: .utf8)
            let jsonData = try decoder.decode(SaveData.self, from: data!)
            SaveValues.values.setData(data: jsonData)
        } catch {
            print("JSON loading error: \(error)")
        }
        
    }
    
    func save () {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("json.txt")
        do {
            let encoder = JSONEncoder()
            let jsonData = try encoder.encode(SaveValues.values.getData())
            let jsonString = String(data: jsonData, encoding: .utf8)
            try jsonString?.write(to: fileUrl, atomically: true, encoding: .utf8)
        } catch {
            print("JSON saving error: \(error)")
        }
    }
}
