//
//  CityWeather.swift
//  dao
//
//  Created by vadim on 24/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

class CityWeather: Codable {
    let coord: Coord
    let weather: [Weather]
    let main: Root
    let name: String
    
    init(coord: Coord, weather: [Weather], main: Root, name: String) {
        self.coord = coord
        self.weather = weather
        self.main = main
        self.name = name
    }
}

class Coord:Codable {
    let lon: Double
    let lat: Double
    
    init(lon: Double, lat:Double) {
        self.lon = lon
        self.lat = lat
    }
}

class Weather:Codable {
    let main: String
    let description: String
    init(main: String, description: String) {
        self.main = main
        self.description = description
    }
}

class Root: Codable {
    var temp: Double
    let temp_min: Double
    let temp_max: Double
    
    init(temp: Double, temp_min: Double, temp_max: Double) {
        self.temp = temp
        self.temp_min = temp_min
        self.temp_max = temp_max
    }
}
