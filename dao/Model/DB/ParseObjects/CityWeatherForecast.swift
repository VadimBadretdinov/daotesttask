//
//  CityWeatherForecast.swift
//  dao
//
//  Created by vadim on 29/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

class CityWeatherForecast: Codable {
    let list: [ForecastCityWeather]
    let city: City
    
    init(weather: [ForecastCityWeather], city: City) {
        self.list = weather
        self.city = city
    }
}

class ForecastCityWeather: Codable {
    let main: Root
    let weather: [Weather]
    let dt_txt: String
    
    init(main: Root, weather: [Weather], dt_txt: String) {
        self.main = main
        self.weather = weather
        self.dt_txt = dt_txt
    }
}

class City: Codable {
    let name: String
    let coord: Coord
    
    init(name: String, coord: Coord) {
        self.name = name
        self.coord = coord
    }
}
