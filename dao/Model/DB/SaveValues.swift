//
//  SaveValues.swift
//  dao
//
//  Created by vadim on 04/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

class SaveValues {
    
    static let values = SaveValues()
    fileprivate var data = SaveData()
    
    func getData() -> SaveData {
        return self.data
    }
    
    func setData(data: SaveData) {
        self.data = data
    }
}

class SaveData: Codable {
    
    fileprivate var activeCity: CityObject?
    fileprivate var cities: [CityObject]?
    
    func setCity(city: CityObject) {
        self.activeCity = city
    }
    
    func getCity() -> CityObject? {
        return self.activeCity
    }
    
    func setCitiesList(cities: [CityObject]) {
        self.cities = cities
    }
    
    func getCitiesList() -> [CityObject]? {
        return self.cities
    }
}
