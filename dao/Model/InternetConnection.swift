//
//  InternetConnection.swift
//  dao
//
//  Created by vadim on 07/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation
import Alamofire

class InternetConnection {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    
}
