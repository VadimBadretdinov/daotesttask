//
//  CitiesTableViewPresenter.swift
//  dao
//
//  Created by vadim on 03/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

class CitiesTableViewPresenter {
    fileprivate weak var citiesTVC: CitiesTableView?
    fileprivate let model = CityWeatherModel()
    fileprivate let jsonManager = JsonStorage.shared
    
    init() {}
    
    func attachView(view: CitiesTableView) {
        self.citiesTVC = view
    }
    
    func detachView() {
        self.citiesTVC = nil
    }
    
    func setCities() {
        self.citiesTVC?.setCities(cities: self.model.getCities())
    }
    
    func setUpdatedCities(city: CityObject) {
        if let updated = self.model.updateCity(update: city) {
                    self.citiesTVC?.setCities(cities: updated)
        }
    }
    
    func addCurrentCityToList(city: CityObject) {
        if let lon = city.lon, let lat = city.lat {
            self.model.fetchCurrentWeather(lon: lon, lat: lat) { (weather) in
                if let currentWeather = weather {
                    city.city = currentWeather.name
                    self.model.appendCurrentCity(city: city)
                    self.citiesTVC?.updateCitiesList(cities: self.model.getCities())
                }
            }
        }
    }

    func setWeather(city: CityObject) {
        var currentWeather: CityWeather?
        var weekWeather: CityWeatherForecast?
        if let lat = city.lat, let lon = city.lon {
            let group = DispatchGroup()
            group.enter()
            self.model.fetchCurrentWeather(lon: lon, lat: lat){ (weather) in
                currentWeather = weather
                group.leave()
            }
            group.enter()
            self.model.fetchWeekWeather(lon: lon, lat: lat) { (weather) in
                weekWeather = weather
                group.leave()
            }
            group.notify(queue: .main) {
                self.citiesTVC?.setWeather(current: currentWeather, week: weekWeather)
            }
        }
    }
    
    func deleteSavedCurrentCity() {
        SaveValues.values.getData().setCity(city: CityObject(city: nil, lon: nil, lat: nil, weather: nil, weekWeather: nil, currentLocation: false))
    }
    
    func saveCitiesList(cities: [CityObject]) {
        SaveValues.values.getData().setCitiesList(cities: cities)
        self.jsonManager.save()
    }
}
