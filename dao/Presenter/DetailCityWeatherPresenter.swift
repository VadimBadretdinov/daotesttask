//
//  DetailCityWeatherPresenter.swift
//  dao
//
//  Created by vadim on 03/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

class DetailCityWeatherPresenter {
    
    fileprivate weak var detailCityWeatherVC: DetailCityWeatherView?
    fileprivate let model = CityWeatherModel()
    fileprivate let jsonManager = JsonStorage.shared
    
    init() {}
    
    func attachView(view: DetailCityWeatherViewController) {
        self.detailCityWeatherVC = view
    }
    
    func detachView() {
        self.detailCityWeatherVC = nil
    }
    
    func saveJsonData(city: CityObject) {
        SaveValues.values.getData().setCity(city: city)
        self.jsonManager.save()
    }
}
