//
//  CitiesTableView.swift
//  dao
//
//  Created by vadim on 03/08/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import Foundation

protocol CitiesTableView: class {
    func setCities(cities: [CityObject])
    func setWeather(current: CityWeather?, week: CityWeatherForecast?)
    func updateCitiesList(cities: [CityObject])
}
