//
//  ViewController.swift
//  dao
//
//  Created by vadim on 23/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit
import JGProgressHUD
import MapKit
import CoreLocation
import Network

class CitiesTableViewController: UIViewController {
    fileprivate let presenter = CitiesTableViewPresenter()
    
    @IBOutlet private var tableView: UITableView!
    
    fileprivate var cities: [CityObject]?
    fileprivate var city: CityObject?
    
    fileprivate let group = DispatchGroup()
    fileprivate let locationManager = CLLocationManager()
    
    fileprivate let hud: JGProgressHUD = {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Downloading..."
        return hud
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        
        self.presenter.deleteSavedCurrentCity()
        if let cities = self.cities {
            self.presenter.saveCitiesList(cities: cities)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(view: self)
        self.presenter.setCities()
        
        self.locationManagerEnable()
        
        if let list = SaveValues.values.getData().getCitiesList() {
            self.cities = list
        }
        
        if let сityObject = SaveValues.values.getData().getCity(),
            let cityFromCO = сityObject.city,
            let cities = self.cities,
            cities.contains(where: { (city) -> Bool in
                
            if let arrayCity = city.city {
               return arrayCity  == cityFromCO
            }
            return false
        }) {
            self.hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
            self.hud.show(in: self.view)
            self.city = сityObject
            self.needDownloadWeather(city: сityObject)
        }
    }
    
    func needDownloadWeather(city: CityObject) {
        if InternetConnection.isConnectedToInternet() {
            self.presenter.setWeather(city: city)
        } else {
            self.haveWeatherValues()
        }
    }
    
    func haveWeatherValues() {
        if self.city?.weather != nil && self.city?.weekWeather != nil {
            hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            hud.dismiss(afterDelay: 0.2)
            self.performSegue(withIdentifier: "weatherSegue", sender: nil)
        } else {
            hud.textLabel.text = "Error: fetch data error"
            hud.indicatorView = JGProgressHUDErrorIndicatorView()
            hud.dismiss(afterDelay: 1)
        }
    }
}

extension CitiesTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cities = self.cities?.count {
            return cities
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        self.hud.show(in: self.view)
        
        if let cities = self.cities {
            let city = cities[indexPath.row]
            self.city = city
            self.needDownloadWeather(city: city)
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        if let cities = self.cities, let city = cities[indexPath.row].city {
            cell.textLabel?.text = city
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DetailCityWeatherViewController{
            vc.currentCity = self.city
            self.city = nil
        }
    }
}

extension CitiesTableViewController: CitiesTableView {
    func updateCitiesList(cities: [CityObject]) {
        self.cities = cities
        self.tableView.reloadData()
    }
    
    func setCities(cities: [CityObject]) {
        self.cities = cities
    }
    
    func setWeather(current: CityWeather?, week: CityWeatherForecast?) {
        
        if let city = self.city, let currentWeather = current, let weekWeather = week {
            city.weather = currentWeather
            city.weekWeather = weekWeather
            self.presenter.setUpdatedCities(city: city)
        }
        self.haveWeatherValues()
    }
}

extension CitiesTableViewController: CLLocationManagerDelegate {
    
    func locationManagerEnable() {
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location else { return }
        locationManager.stopUpdatingLocation()
        self.presenter.addCurrentCityToList(city: CityObject(city: nil, lon: location.coordinate.longitude, lat: location.coordinate.latitude, weather: nil, weekWeather: nil, currentLocation: true))
    }
}
