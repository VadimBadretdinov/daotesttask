//
//  WeekWeatherTableViewCell.swift
//  dao
//
//  Created by vadim on 24/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit
import SwiftDate

class WeekWeatherTableViewCell: UITableViewCell {

    private let weatherService = WeatherService()
    
    //morning
    @IBOutlet private var morningImage: UIImageView!
    @IBOutlet private var morningTemp: UILabel!
    
    //afternoon
    @IBOutlet private var afternoonImage: UIImageView!
    @IBOutlet private var afternoonTemp: UILabel!
    
    //night
    @IBOutlet private var nightImage: UIImageView!
    @IBOutlet private var nightTemp: UILabel!
    
    @IBOutlet private var city: UILabel!
    @IBOutlet private var day: UILabel!
    
    var cityObject: CityObject?
    
    func setupCell() {
        guard let object = self.cityObject, let weatherList = object.weekWeather?.list else {
            return
        }
        
        for i in 0..<weatherList.count {
            if let date2 = weatherList[i].dt_txt.toDate(),
                let objDay = object.day {
                if (date2.day - objDay) == 1 && (weatherList.count - i) >= 8 {
                    
                    self.morningTemp.text = "\(weatherList[i+2].main.temp.toCelsiusIntValue())"
                    self.afternoonTemp.text = "\(weatherList[i+4].main.temp.toCelsiusIntValue())"
                    self.nightTemp.text = "\(weatherList[i+7].main.temp.toCelsiusIntValue())"
                    
                    self.city.text = object.city
                    self.day.text = date2.weekdayName(.default)
                    
                    if let morning = weatherList[i+2].weather.first, let weatherMorning = TodayWeather(rawValue: morning.main),
                        let afternoon = weatherList[i+4].weather.first, let weatherAfternoon = TodayWeather(rawValue: afternoon.main),
                        let night = weatherList[i+7].weather.first, let weatherNight = TodayWeather(rawValue: night.main) {
                        
                        self.morningImage.image = self.weatherService.getWeatherIcon(weather: weatherMorning, time: .Day)
                        self.afternoonImage.image = self.weatherService.getWeatherIcon(weather: weatherAfternoon, time: .Day)
                        self.nightImage.image = self.weatherService.getWeatherIcon(weather: weatherNight, time: .Night)
                        
                    }
                }
            }
        }
    }
}
