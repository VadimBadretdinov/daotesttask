//
//  DetailCityWeatherViewController.swift
//  dao
//
//  Created by vadim on 24/07/2019.
//  Copyright © 2019 vadim. All rights reserved.
//

import UIKit
import SwiftDate

class DetailCityWeatherViewController: UIViewController {
    fileprivate let presenter = DetailCityWeatherPresenter()
    fileprivate let weatherService = WeatherService()
    
    @IBOutlet private var currentWeatherImage: UIImageView!
    @IBOutlet private var cityLabel: UILabel!
    @IBOutlet private var engCityLabel: UILabel!
    @IBOutlet private var currentTempLabel: UILabel!
    @IBOutlet private var tableViewWeekWeather: UITableView!
    
    var currentCity:CityObject?
    
    fileprivate var weatherDay = DateInRegion()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(view: self)
        if let city = self.currentCity {
            self.presenter.saveJsonData(city: city)
        }
        self.setupCurrentWeather()
    }
    
    fileprivate func setupCurrentWeather() {
        if let cCity = currentCity, let city = cCity.city, let weather = cCity.weather {
            self.cityLabel.text = city
            self.currentTempLabel.text = "\(weather.main.temp.toCelsiusIntValue())"
            if self.weatherDay.hour >= 21 {
                if let weather = weather.weather.first, let weatherString = TodayWeather(rawValue: weather.main) {
                    self.currentWeatherImage.image = self.weatherService.getWeatherIcon(weather: weatherString, time: .Night)
                }
            } else {
                if let weather = weather.weather.first, let weatherString = TodayWeather(rawValue: weather.main) {
                   self.currentWeatherImage.image = self.weatherService.getWeatherIcon(weather: weatherString, time: .Day)
                }

            }
        }
    }

}

extension DetailCityWeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCityWeather", for: indexPath) as? WeekWeatherTableViewCell else { return UITableViewCell() }
        
        if let cityobj = self.currentCity {
            cityobj.day = DateInRegion().day + indexPath.row
            cell.cityObject = cityobj
            cell.setupCell()
        }
        return cell
    }
}

extension DetailCityWeatherViewController: DetailCityWeatherView {
    
}
